'use strict';

// Define the `phonecatApp` module
var app = angular.module('mainApp', ['ui.router']);

app.config(function($stateProvider, $urlRouterProvider){
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('home',{
            url: '/',
            templateUrl: 'home.html'
        })
        .state('posts', {
            url: '/posts',
            template: '<ui-view></ui-view>'
        })
        .state('posts.incomplete', {
            url: '/incomplete',
            template: '<posts-list posts = "vm.incompletePosts"></posts-list>',
            controllerAs: 'vm'
        })
        .state('posts.complete', {
            url: '/complete',
            template: '<posts-list posts = "vm.incompletePosts"></posts-list>',
            controllerAs: 'vm'
        })
})
app.controller('mainCtrl', function(mainSvc) {
    var vm = this;
    this.server = 'https://jsonplaceholder.typicode.com/posts';
    this.hello = "hello";
    this.name = "alex";
    this.fruits = ['banana', 'oranges', 'kiwi'];
    this.alertMe = function(){
        alert("Hello dude!")
    };


    mainSvc.getPosts().then(function(response){
        this.incompletePosts = vm.response.data.splice(0,50);
        this.completePosts = vm.response.data;

    })

});

app.filter('makePlural', function(){
    return function(input){
        return input + "s"
    }
})

app.service('mainSvc', function($http){
    this.getPosts = function(){
        return $http.get('https://jsonplaceholder.typicode.com/posts')
    };
})